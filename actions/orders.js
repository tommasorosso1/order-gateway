const orderStates = require('../states/orderStates').orderStates;
const Action = require('./Action');

exports.processOrder = order => Action.create(orderStates.process, order);

exports.deleteOrder = id => Action.create(orderStates.delete, id);

exports.completeOrder = order => Action.create(orderStates.complete, order);
