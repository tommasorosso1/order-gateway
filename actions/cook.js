const cookStates = require('../states/cookStates').cookStates;
const Action = require('./Action');

exports.present = data => Action.create(cookStates.present, data);

exports.absent = data => Action.create(cookStates.absent, data);
