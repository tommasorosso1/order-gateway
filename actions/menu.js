const menuStates = require('../states/menuStates').menuStates;
const Action = require('./Action');

exports.addDish = dish => Action.create(menuStates.add, dish);

exports.removeDish = id => Action.create(menuStates.remove, id);

exports.editDish = newDish => Action.create(menuStates.modify, newDish);
