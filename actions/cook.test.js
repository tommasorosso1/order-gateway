const Action = require('./Action');
const cookActions = require('./cook');
const cookStates = require('../states/cookStates').cookStates;
const expect = require('chai').expect;

describe('Cook actions test suite', () => {
  const data = {
    cookId: 0,
    dateTime: new Date(),
  };

  it('should create an action that signals cook presence', () => {
    const expectedAction = Action.create(cookStates.present, data);
    expect(cookActions.present(data)).to.deep.equal(expectedAction);
  });

  it('should create an action that signals cook absence', () => {
    const expectedAction = Action.create(cookStates.absent, data);
    expect(cookActions.absent(data)).to.deep.equal(expectedAction);
  });
});
