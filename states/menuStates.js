exports.menuStates = {
  add: 'ADD_ELEMENT',
  modify: 'MODIFY_ELEMENT',
  remove: 'REMOVE_ELEMENT',
};
