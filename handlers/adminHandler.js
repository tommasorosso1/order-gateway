const menuActions = require('../actions/menu');
const orderActions = require('../actions/orders');

exports.adminHandler = (socket, store) => {
  // GESTIONE DELLE RICHIESTE DELLA BOLLA
  console.log('admin connesso');
  socket.on('disconnect', () => {
    console.log('admin disconnesso');
  });

  // REAZIONI ALLE RICHIESTE
  // vedere il menu
  socket.on('menu', () => {
    socket.emit('menu', store.getState().menu.dishes);
  });
  // aggiungere piatti al menu
  socket.on('addDish', (dish) => {
    store.dispatch(menuActions.addDish(dish).asPlainObject());
    socket.emit('addedDish', dish);
  });
  // togliere piatti dal menu
  socket.on('removeDish', (id) => {
    store.dispatch(menuActions.removeDish(id).asPlainObject());
    socket.emit('removeDish', id);
  });
  // modificare un piatto dal menu
  socket.on('editDish', (payload) => {
    store.dispatch(menuActions.editDish(payload).asPlainObject());

    const filterFunction = element => element.id === payload.id;
    const dishes = store.getState().menu.dishes;

    socket.emit('editDish', dishes.filter(filterFunction));
  });
  // vedere gli ordini
  socket.on('allOrders', () => {
    socket.emit('allOrders', store.getState().order.orders);
  });
  // vedere gli ordini attivi
  socket.on('activeOrders', () => {
    const filterFunction = element => element.state === 'active';
    const orders = store.getState().order.orders;
    socket.emit('activeOrders', orders.filter(filterFunction));
  });
  // vedere gli ordini non attivi
  socket.on('completedOrders', () => {
    const filterFunction = element => element.state !== 'active';
    const orders = store.getState().order.orders;
    socket.emit('completedOrders', orders.filter(filterFunction));
  });
  // eliminare un ordine
  socket.on('deleteOrder', (id) => {
    store.dispatch(orderActions.deleteOrder(id).asPlainObject());
    // che sia il caso di marcarlo come eliminato invece che cancellarlo veramente?
    // nik: secondo me si potrebbe creare una collection apposta (come per gli ordini completati)
    socket.emit('deleteOrder', id);
  });
};
