const cookActions = require('../actions/cook');
const orderActions = require('../actions/orders');

exports.cookHandler = (socket, store, orders, addToDatadaseCompletedOrder) => {
  // nel caso che ci siano ordinazioni attive nello stato nel frattempo che il cuoco non e'
  // connesso viene notificato con le ordinazioni
  const activeFilterFunction = element => element.state === 'active';
  let ordersInState = store.getState().order.orders;
  let activeOrders = ordersInState.filter(activeFilterFunction);
  socket.on('ready', () => {
    if (activeOrders.length !== 0) {
      socket.emit('activeOrdinations', activeOrders);
    }
  });
  // appena connesso viene registrato nello stato che il cuoco e' presente
  store.dispatch(cookActions.present(new Date()).asPlainObject());
  console.log('cook connected');
  // gestione dell'evento di disconnessione del cuoco.. viene settato lo stato corrispondente
  socket.on('disconnect', () => {
    store.dispatch(cookActions.absent(new Date()).asPlainObject());
  });

  // attiva la funzionalita' di listener sulle modifiche dello stato dell'app..
  // in particolare se avvengono nuovi ordini li notifica al cuoco
  store.subscribe(() => {
    ordersInState = store.getState().order.orders;
    activeOrders = ordersInState.filter(activeFilterFunction);
    socket.emit('activeOrdinations', activeOrders);
  });

  // quando la bolla del cuoco notifica che e' stato preparato il piatto relativo all'id specificato
  // viene creato un evento con nome = id in modo da poter notificare il cliente corrispondente
  // che il suo ordine e' pronto
  socket.on('orderCompleted', (id) => {
    orders.emit(id);//
    // lo salvo completato sullo stato;
    store.dispatch(orderActions.completeOrder(id).asPlainObject());
    // salvo sul database l'ordinazione effettuata
    // essendo l'id univoco e' per forza quello che voglio io (primo risultato)
    const order = ordersInState.filter(element => element.id === id)[0];
    if (typeof order !== 'undefined') addToDatadaseCompletedOrder(order);
  });
};
