const server = require('../server/server').create();
const adminHandler = require('./adminHandler').adminHandler;
const assert = require('assert');
const config = require('../configuration/config');
const socketClient = require('socket.io-client');

const serverUrl = config.getTestServerURL();
const io = server.getSocket();

const fakeDish = {
  id: 0,
  name: 'pasta al ragu',
  price: 8,
  description: 'pasta al ragu',
};

const store = {
  getState: () => ({
    menu: {
      dishes: ['ciaoz'],
    },
    order: {
      orders: [],
    },
  }),
  dispatch: () => {},
};

describe('admin handler responses to events', () => {
  const response = {};
  let client;
  before((done) => {
    server.open(config.test.port);

    client = socketClient(serverUrl);
    io.on('connection', (socket) => {
      adminHandler(socket, store);
      // risposte
      client.on('menu', (data) => {
        response.menuEvent = data;
      });
      client.on('addedDish', (dish) => {
        response.addDishEvent = dish;
      });
      client.on('editDish', () => {
        response.editDishEvent = true;
      });
      client.on('removeDish', (data) => {
        response.removeDishEvent = data;
      });
      client.on('allOrders', (data) => {
        response.allOrdersEvent = data;
      });
      client.on('activeOrders', (data) => {
        response.activeOrdersEvent = data;
      });
      client.on('completedOrders', (data) => {
        response.completedOrdersEvent = data;
      });
      client.on('deleteOrder', (data) => {
        response.deleteOrderEvent = data;
      });

      // richieste
      setTimeout(() => {
        client.emit('menu');
        client.emit('addDish', fakeDish);
        client.emit('editDish', fakeDish);
        client.emit('removeDish', 0);
        client.emit('allOrders');
        client.emit('activeOrders');
        client.emit('completedOrders');
        client.emit('deleteOrder', 1);

        // gli diamo il tempo di finire
        setTimeout(() => {
          server.close();
          done();
        }, 1000);
      }, 50);
    });
  });

  // verifico che vada tutto bene
  it('menuEvent', () => {
    assert.deepEqual(response.menuEvent, store.getState().menu.dishes);
  });
  it('addDishevent', () => {
    assert.deepEqual(response.addDishEvent, fakeDish);
  });
  it('editDishEvent', () => {
    assert.equal(response.editDishEvent, true);
  });
  it('removeDishEvent', () => {
    assert.equal(response.removeDishEvent, 0);
  });
  it('allOrdersEvent', () => {
    assert.deepEqual(response.allOrdersEvent, store.getState().order.orders);
  });
  it('activeOrdersEvent', () => {
    assert.deepEqual(response.activeOrdersEvent, []);
  });
  it('completedOrdersEvent', () => {
    assert.deepEqual(response.completedOrdersEvent, []);
  });
  it('deleteOrderEvent', () => {
    assert.deepEqual(response.deleteOrderEvent, 1);
  });
});

