// .....................................IMPORT....................................................
// import configuration
const config = require('./configuration/config');

// import server components
const app = require('express')();
const server = require('http').createServer(app);
const io = require('socket.io')(server);

// import redux createState function
const createStore = require('redux').createStore;

// import various connections handlers
const cookHandler = require('./handlers/cookHandler').cookHandler;
const clientHandler = require('./handlers/clientHandler').clientHandler;
const adminHandler = require('./handlers/adminHandler').adminHandler;

// import EventEmitter (manage communications between clients and server)
const EventEmitter = require('events');

// import redux reducers
const reducers = require('./reducers/combineReducer').reducers;

// import mongo client
const MongoClient = require('mongodb').MongoClient;

// create an EventEmitter used for orders
class Orders extends EventEmitter {}
const orders = new Orders();

// increase concurrent operations limit
orders.setMaxListeners(500);

const mongoUrl = config.production.mongoUrl;

// initialize an empty redux store
let store = {};

// ....................................STATE INITIALIZATION & DB OPERATIONS.........................
// function used to persist state on DB (collection 'state')
// if collection is not there, it's automatically created with the current state:
// {"name":"state", "state":{lo stato dell' app}}
const saveStateToDB = () => {
  MongoClient.connect(mongoUrl).then((database) => {
    // console.log('DB connected successfully');
    const state = database.collection('state');
    state.findOneAndUpdate(
      { name: 'state' },
      { $set: { state: store.getState() } },
      { returnOriginal: false, upsert: true }).then(() => {
        console.log('state saved into database');
      }).catch((err) => { console.log(err); });
    database.close();
  }).catch((err) => {
    console.log(err);
  });
};

// function used to save a completed order in the appropriate collection (collection 'orders')
const addCompletedOrderToDB = function (ordination) {
  MongoClient.connect(mongoUrl).then((database) => {
    const ordersCollection = database.collection('orders');
    ordersCollection.insertOne(ordination).then(() => {
      console.log('Completed order saved to database');
    }).catch((err) => {
      console.log(err);
      console.log('Failed to save completed order to database');
    });
    database.close();
  }).catch(() => {
    console.error('DB connection failed.');
  });
};


// Redux store creation
// 3 cases
/*  1) connection to DB successful
      1.A) "state" collection present => load state from DB
      1.B) "state" collection not found => initialize default state and persist it immediately
      BOTH) set store's subscriber to saveStateToDB (keep state updated also on DB)

    2) connection to DB failed => run without persistence: initialize default state but not save it to DB.
       TODO DEBUG PURPOSES ONLY
*/

// try to connect to db
MongoClient.connect(mongoUrl).then((db) => {
  const collection = db.collection('state');
  collection.findOne().then((state) => {
    // active orders filter function
    const activeOrders = element => element.state === 'active';

    // DB connected and state found -> 1.A
    // retrieve all active orders
    state.state.order.orders = state.state.order.orders.filter(activeOrders);
    store = createStore(reducers, state.state);
    store.subscribe(saveStateToDB);
    console.log(`Server state loaded from DB at ${mongoUrl}`);
  }).catch((err) => {
    console.error(err);
    // DB connected but state not found => 1.B
    console.log('DB is not initialized.. initialising it now');
    store = createStore(reducers);
    store.subscribe(saveStateToDB);
  });
  db.close();
}).catch(() => {
  // could not connect to DB => 2
  const offlineWarning = 'Working in offline mode (DEBUG ONLY)';
  console.log(`Cannot connect to DB at ${mongoUrl}. ${offlineWarning}`);
  // Default store
  store = createStore(reducers);
  store.subscribe(() => {
    // trace what's happening for debug
    console.log('Server state modified:', store.getState());
  });
});

// run server
server.listen(config.production.port);
console.log('Server is running');


// .....................................CONNECTION EVENTS.....................................

// route each client to its specific connectionHandler
// TODO authentication goes HERE

io.on('connection', (socket) => {
  socket.on('auth', (authData) => {
    if (authData.type === 'cook') {
      cookHandler(socket, store, orders, addCompletedOrderToDB);
    } else if (authData.type === 'client') {
      clientHandler(socket, store, orders);
    } else if (authData.type === 'admin') {
      adminHandler(socket, store);
    }
  });
});
