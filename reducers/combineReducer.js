const menuReducer = require('./menuReducer').menuReducer;
const cookReducer = require('./cookReducer').cookReducer;
const orderReducer = require('./orderReducer').orderReducer;
const combineReducers = require('redux').combineReducers;
// combine reducers

exports.reducers = combineReducers({
  cook: cookReducer,
  order: orderReducer,
  menu: menuReducer,
});
