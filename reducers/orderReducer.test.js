const orderReducer = require('./orderReducer').orderReducer;
const orderStates = require('../states/orderStates').orderStates;
const assert = require('assert');

const state = {
  nextID: 0,
  orders: [],
};

describe('order reducer on orderStates.process ', () => {
  const newState = orderReducer(
    state,
    {
      type: orderStates.process,
      payload: {},
    });

  it('should have raised nextID by 1', () => {
    assert.equal(newState.nextID, 1);
  });
  it('should have an active ordination', () => {
    assert.equal(newState.orders[0].state, 'active');
  });
  it('should have an active ordination with id 0', () => {
    assert.equal(newState.orders[0].id, 0);
  });
});

describe('order reducer on orderStates.complete ', () => {
  const newState = orderReducer(
    state,
    {
      type: orderStates.process,
      payload: {},
    });

  it('should have marked the 0 ordination completed', () => {
    assert.equal(orderReducer(
      newState,
      {
        type: orderStates.complete,
        payload: 0,
      }).orders[0].state, 'completed');
  });
});

describe('order reducer on orderStates.delete ', () => {
  const newState = orderReducer(
    state,
    {
      type: orderStates.process,
      payload: {},
    });

  it('should have marked the 0 ordination completed', () => {
    assert.equal(orderReducer(newState,
      {
        type: orderStates.delete,
        payload: 0,
      }).orders.length, 0);
  });
});
