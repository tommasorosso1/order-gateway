const orderStates = require('../states/orderStates').orderStates;

// reducer per lo stato degli ordini
exports.orderReducer = (state = { nextID: 0, orders: [] }, action) => {
  // AL MOMENTO C'E' QUESTO WARNING ESLINT: http://eslint.org/docs/rules/no-param-reassign
  // DOVREMO CERCARE UN MODO PER EVITARE DI MODIFICARE IL PAYLOAD DELLA ACTION DIRETTAMENTE MI SA...
  let newState = Object.assign({}, state);
  switch (action.type) {
    case orderStates.process:
      action.payload.state = 'active';
      action.payload.id = state.nextID;
      newState = Object.assign({}, state, { nextID: state.nextID + 1 });
      newState.orders.push(action.payload);
      return newState;
    case orderStates.complete:
      // payload {int}
      const mapFunction = (element) => {
        if (element.id === action.payload) {
          element.state = 'completed';
          return element;
        }
      };

      newState = Object.assign({}, state);
      newState.orders.map(mapFunction);
      return newState;
    case orderStates.delete:
      // payload = int id dell'elemento da togliere
      // PRE: l'elemento corrispondente e' presente se non lo e' non vengono fatte modifiche allo stato.
      const filterFunction = element => element.id !== action.payload;
      return Object.assign({}, state, {
        orders: state.orders.filter(filterFunction),
      });
      // POST: nel nuovo stato non e' piu presente l'elemento
    default:
      return state;
  }
};
