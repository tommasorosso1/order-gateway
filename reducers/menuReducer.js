const menuStates = require('../states/menuStates').menuStates;

// struttura standard di un oggetto menu
const defaultMenuState = {
  dishes: [],
  date: new Date(),
  nextID: 0,
};

// reducer per lo stato del menu
exports.menuReducer = (state = defaultMenuState, action) => {
  // AL MOMENTO C'E' QUESTO WARNING ESLINT: http://eslint.org/docs/rules/no-param-reassign
  // DOVREMO CERCARE UN MODO PER EVITARE DI MODIFICARE IL PAYLOAD DELLA ACTION DIRETTAMENTE MI SA...
  switch (action.type) {
    case menuStates.add: {
      // payload ={name: string, price: double, descriiption: string}
      // viene assegnato un id univoco e il piatto viene aggiunto al menu
      action.payload.id = state.nextID;
      const newState = Object.assign({}, state, { nextID: state.nextID + 1 });
      newState.dishes.push(action.payload);
      return newState;
    }
    case menuStates.remove: {
      // payload =  int id dell' elemento da togliere
      // PRE: l'elemento corrispondente e' presente
      //      se non lo e' non vengono fatte modifiche allo stato.
      const filterFunction = element => element.id !== action.payload;
      return Object.assign({}, state, {
        dishes: state.dishes.filter(filterFunction),
      });
      // POST: nel nuovo stato non e' piu presente l'elemento
    }
    case menuStates.modify: {
      // parametri da mandare su:
      // payload = {id: int, dish:{name: string, price: double, descriiption: string}}
      // payload.id e' l' id dell' elemento da modificare,
      // payload.dish e' il nuovo piatto che ne prende il posto
      // PRE: l'elemento corrispondente a payload.id deve essere presente altrimenti non vengono fatte modifiche
      const mapFunction = (element) => {
        if (element.id !== action.payload.id) return element;
        action.payload.dish.id = action.payload.id;
        return action.payload.dish;
      };
      return Object.assign({}, state, { dishes: state.dishes.map(mapFunction) });
      // POST al posto dell'elemento con id = a payload.id c'e' l'elemento presente in payload.dish
    }
    default:
      return state;
  }
};
