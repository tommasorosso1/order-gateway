const cookReducer = require('./cookReducer').cookReducer;
const cookStates = require('../states/cookStates').cookStates;
const assert = require('chai').assert;
const expect = require('chai').expect;

const initialState = {
  present: false,
};

describe('cook reducer', () => {
  const state = undefined;

  it('should return the initial state', () => {
    expect(cookReducer(state, {})).to.deep.equal(initialState);
  });

  it('should be set present on cookStates.present action', () => {
    const newState = cookReducer(state, { type: cookStates.present });
    assert.isTrue(newState.present, 'cook is present');
  });

  it('should be set absent on cookStates.absent action', () => {
    const newState = cookReducer(state, { type: cookStates.absent });
    assert.isFalse(newState.present, 'cook is absent');
  });
});
