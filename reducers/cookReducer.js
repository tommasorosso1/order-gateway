// reducer per lo stato del cuoco
const cookStates = require('../states/cookStates').cookStates;

exports.cookReducer = (state = { present: false }, action) => {
  switch (action.type) {
    case cookStates.present:
      return Object.assign({}, state, { present: true });
    case cookStates.absent:
      return Object.assign({}, state, { present: false });
    default:
      return state;
  }
};
